**************
Notejam: Flask
**************

Forked from `komarserjio/notejam <https://github.com/komarserjio/notejam>`_

Notejam application implemented using `Flask <http://flask.pocoo.org/>`_ microframework.

Flask version: 0.9

Flask extension used:

* Flask-Login
* Flask-Mail
* Flask-SQLAlchemy
* Flask-Testing
* Flask-WTF

==========================
Installation and launching
==========================

-----
Clone
-----

Clone the repo:

.. code-block:: bash

    $ git clone https://gitlab.com/nhan-assignments/pp-app.git

----------------------------------
Install, run test cases and launch
----------------------------------

Use `virtualenv <http://www.virtualenv.org>`_ or `virtualenvwrapper <http://virtualenvwrapper.readthedocs.org/>`_
for `environment management <http://docs.python-guide.org/en/latest/dev/virtualenvs/>`_.

Install dependencies, test functional and unit tests and launch app:

.. code-block:: bash

    $ cd pp-app/
    $ make all

Go to http://127.0.0.1:5000/ in your browser.

Specified target, please take a look at `Makefile`

.. code-block:: bash

    $ make
