#@IgnoreInspection BashAddShebang
export ROOT=$(realpath $(dir $(lastword $(MAKEFILE_LIST))))
export VENV_DIR := $(abspath $(dir $(lastword $(MAKEFILE_LIST)))/../venv)
export VENV_BIN_DIR := $(abspath $(dir $(lastword $(MAKEFILE_LIST)))/../venv/bin)
export APP=notejam

.PHONY all: ## Reinstall libs and run unit tests
all: build test run

init-local-dev: ## Init local dev env
	virtualenv -p /usr/bin/python2.7 $(VENV_DIR)

build: ## Reinstall requirements.txt
	$(VENV_BIN_DIR)/pip install -r requirements.txt

run: ## run app "notejam"
	$(VENV_BIN_DIR)/python runserver.py

test: ## Run functional and unit test
	$(VENV_BIN_DIR)/python tests.py

container: ## Containerize notejam app
	docker build -t $(APP) .

run-container: ## Start container
	docker run -p 5000:5000 --rm -it $(APP)

ci-test: ## Run functional and unit test CI pipelines
	pip install -r requirements.txt
	python tests.py

ci-deliver: ## This target must only run in gitlab CI - deliver docker image
	docker build -t bol0bal4/$(APP):$(CI_COMMIT_SHORT_SHA) .
	docker login $(CI_REGISTRY) -u $(CI_REGISTRY_USER) -p $(CI_REGISTRY_PASSWORD)
	docker push bol0bal4/$(APP):$(CI_COMMIT_SHORT_SHA)

.PHONY: help
help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.DEFAULT_GOAL := help
